# Faircamp Manual

The manual is organized into *topics* and *reference* pages.

## Topics

For an introduction go through the topics in order. These show how faircamp
works in general and swiftly guide you to your first faircamp site.

## Reference

Beyond that, the reference pages then explain all available options and
metadata that you can set in your manifests.

## Other resources

- See the [website](https://simonrepp.com/faircamp) for screenshots.
- See the [repository](https://codeberg.org/simonrepp/faircamp) for the source code.
