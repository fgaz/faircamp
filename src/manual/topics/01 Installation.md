# Installation

On debian-based Linux faircamp can already be installed via packages
(amd64). On Arch Linux and Manjaro faircamp is available through the AUR. For
RPM-based distros copy/paste build and install instructions are available. On
BSD, macOS and Windows you are entering uncharted territory, but in general
there should be no hard blockers for running faircamp on these platforms
either, general build instructions for this are provided.

**Arch Linux, Manjaro**

Install [faircamp-git](https://aur.archlinux.org/packages/faircamp-git) from the AUR

**Debian 12, Ubuntu 23.04**

Install the [.deb package (amd64) [libvips-enabled]](https://simonrepp.com/faircamp/packages/faircamp_427.aabc1b4706-1+deb12_amd64.deb)

**Debian 11, elementary OS, Linux Mint, Ubuntu 22.04 LTS - 22.10**

Install the [.deb package (amd64)](https://simonrepp.com/faircamp/packages/faircamp_427.aabc1b4706-1+deb11_amd64.deb)

**Fedora, CentOS, RHEL**

See copy/paste instructions for these platforms in [Building from source](https://codeberg.org/simonrepp/faircamp/src/branch/main/BUILD.md)

**Other platforms**

See general build instructions in [Building from source](https://codeberg.org/simonrepp/faircamp/src/branch/main/BUILD.md)