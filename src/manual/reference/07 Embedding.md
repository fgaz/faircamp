# Embedding

> Heads up: Embeds are still work in progress - for the moment they will look weird or not work at all.

This allows external sites to embed a widget that presents music from your site.
The embed code can be copied from each release page where embedding is enabled.

Embedding is enabled by default, but you need to set the catalog's `base_url`
for the feature to show up. You can use `disabled` to disable it and
`enabled` to re-enable it for specific albums.

```eno
# embedding

disabled
```