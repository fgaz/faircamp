# Streaming

Always enabled.

You can optionally set the encoding quality for streaming from `standard`
(the default) to `frugal`. This uses considerably less bandwidth, reduces
emissions and improves load times for listeners, especially on slow
connections.

```eno
# streaming

quality: frugal
```