# Payment

This sets payment options that are shown when someone wants to buy one of your
releases. For liberapay just provide your account name. Functionality and
options are still a bit bare here, but for now you can provide any custom
links and information through the `custom` field (which supports markdown).

```eno
# payment

liberapay: ThatAwesomeArtist42

-- custom
I'm playing a show at *Substage Indenhoven* on Dec 19th - you can get the
digital album now and meet me at the merch stand in december in person to give
me the money yourself!
-- custom

-- custom
If you're in europe you can send the money via SEPA, contact me at
[lila@thatawesomeartist42.com](mailto:lila@thatawesomeartist42.com) and I'll
send you the account details.
-- custom
```