use std::cell::RefCell;
use std::rc::Rc;

use crate::{Image, Permalink, Release};

#[derive(Debug)]
pub struct Artist {
    pub aliases: Vec<String>,
    pub image: Option<Rc<RefCell<Image>>>,
    pub name: String,
    pub permalink: Permalink,
    pub releases: Vec<Rc<RefCell<Release>>>,
    pub text: Option<String>
}

impl Artist {
    pub fn new(name: &str) -> Artist {
        let permalink = Permalink::generate(name);
        
        Artist {
            aliases: Vec::new(),
            image: None,
            name: name.to_string(),
            permalink,
            releases: Vec::new(),
            text: None
        }
    }
}

#[derive(Debug)]
pub struct Link {
    pub label: String,
    pub url: String
}