use hound::{SampleFormat, WavReader};
use std::path::Path;

use super::{DecodeResult, I24_MAX};

pub fn decode(path: &Path) -> Option<DecodeResult> {
    let mut reader = match WavReader::open(path) {
        Ok(reader) => reader,
        Err(_) => return None
    };
    
    let sample_count = reader.duration();
    let spec = reader.spec();
    
    let mut result = DecodeResult {
        channels: spec.channels,
        duration: sample_count as f32 / spec.sample_rate as f32,
        sample_count,
        sample_rate: spec.sample_rate,
        samples: Vec::with_capacity(sample_count as usize * spec.channels as usize)
    };
    
    match (spec.sample_format, spec.bits_per_sample) {
        (SampleFormat::Float, _) => for sample in reader.samples::<f32>() {
            result.samples.push(sample.unwrap());
        }
        (SampleFormat::Int, 8) => for sample in reader.samples::<i8>() {
            result.samples.push(sample.unwrap() as f32 / std::i8::MAX as f32);
        }
        (SampleFormat::Int, 16) => for sample in reader.samples::<i16>() {
            result.samples.push(sample.unwrap() as f32 / std::i16::MAX as f32);
        }
        (SampleFormat::Int, 24) => for sample in reader.samples::<i32>() {
            result.samples.push(sample.unwrap() as f32 / I24_MAX as f32);
        }
        (SampleFormat::Int, 32) => for sample in reader.samples::<i32>() {
            result.samples.push(sample.unwrap() as f32 / std::i32::MAX as f32);
        }
        _ => unimplemented!()
    }
    
    Some(result)
}