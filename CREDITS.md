# Credits

An incomplete listing of people who have contributed to faircamp
in one way or another, in no particular order (thus alphabetical).
If someone is missing please let it be known, same if you would
rather not be listed here, no problem, just say the word.

Thanks everyone for your support.

[ailepet](https://codeberg.org/ailepet)<br>
[Dave Lane](https://davelane.nz)<br>
[Dave Riedstra](https://daveriedstra.com/)<br>
[Esi Jóhannes G.](https://esi.is/)<br>
[Francesco Gazzetta](https://codeberg.org/fgaz)<br>
[fREW Schmidt](https://blog.afoolishmanifesto.com/)<br>
[Friedrich](https://codeberg.org/Friedrich)<br>
[GoodTech](https://codeberg.org/GoodTech)<br>
[jd42](https://codeberg.org/jd42)<br>
[Meljoann](https://www.meljoann.com/)<br>
[ncc1988](https://codeberg.org/ncc1988)<br>
[nOOq](https://codeberg.org/n00q)<br>
[Simon Repp](https://simonrepp.com)<br>
[Samæ](https://samae.demer.se/)
